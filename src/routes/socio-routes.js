const express = require('express');

const socioController = require('../controllers/cliente-controller');

const router = express.Router();

router.get('/csv', socioController.findAddressRcursive);

module.exports = router;
