const movimientoRoutes = require('./movimiento-routes');
const socioRoutes = require('./socio-routes');

module.exports = (app) => {
    app.use('/api/v1/movimiento', movimientoRoutes);
    app.use('/api/v1/socio', socioRoutes);
};
