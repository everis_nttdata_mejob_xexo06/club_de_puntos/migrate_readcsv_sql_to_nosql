const express = require('express');

const movimientoController = require('../controllers/movimiento-controller');

const router = express.Router();

router.get('/csv', movimientoController.findAddressRcursive);



module.exports = router;