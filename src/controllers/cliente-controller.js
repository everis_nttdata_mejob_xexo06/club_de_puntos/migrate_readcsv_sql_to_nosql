const socioModel = require('../mongo/models/cliente');
const csvtojson = require('csvtojson');
const path = require('path');

const findAddressRcursive = async (data, dateStartProcces) => {
    try {
        var contador;
        var next;
        var limit;

        const directoryPath = path.join(__dirname, '../export_clientes.csv');

        if (data.lastAfter) {
            //console.log('data.lastAfter : ' + data.lastAfter);
            contador = data.lastAfter;
        } else {
            //console.log('no trae datos ');
            contador = 0;
        }
        console.log('contador' + contador);
        if (data.next) {
            // console.log('data.next : ' + data.next);
            next = data.next;
        } else {
            //console.log('no trae datos ');
            next = true;
        }

        if (data.limit) {
            //console.log('data.limit ' + data.limit);
            limit = data.limit;
        } else {
            // console.log('no trae datos ');
            limit = 5000;
        }

        csvtojson()
            .fromFile(directoryPath)
            .then((source) => {
                while (contador <= limit && next === true) {
                    console.log(source.length);
                    if (680499 === contador) {
                        //if (source.length === contador) {
                        //if (contador === 10) {
                        next = false;
                        var f = new Date();
                        console.log('START PROCCESS: ' + dateStartProcces);
                        console.log('FIN: ' + f);
                        console.log('contador: ' + contador);
                    } else {
                        let datosSocios = new socioModel();
                        datosSocios.rutCliente = source[contador]['RUT_SOCIO'];
                        datosSocios.saldo = source[contador]['SALDO'];

                        console.log(source[contador]['FECHA_CAMBIO_ESTADO']);
                        fechaInvalida =
                            source[contador]['FECHA_CAMBIO_ESTADO'].split(' ');
                        fechaInvalidaParte1 = fechaInvalida[0].split('/');

                        fechaInvalidaParte2 = fechaInvalida[1];
                        fechaValida =
                            '20' +
                            fechaInvalidaParte1[2] +
                            '-' +
                            fechaInvalidaParte1[1] +
                            '-' +
                            fechaInvalidaParte1[0] +
                            'T' +
                            fechaInvalidaParte2;
                        //console.log('fecha valida ' + fechaValida);
                        datosSocios.fechaultimaMod = new Date();

                        datosSocios.fechaCreacion = new Date(fechaValida);
                        datosSocios.estadoMigrado = 'M';

                        datosSocios.save();

                        contador++;
                        //console.log('contador' + contador);
                    }
                }

                if (next === true) {
                    let variables = {
                        lastAfter: contador,
                        next: true,
                        limit: 200 + limit,
                    };
                    findAddressRcursive(variables, dateStartProcces);
                }
            });
    } catch (error) {
        console.log(error);
    }
};

module.exports = {
    findAddressRcursive,
};
