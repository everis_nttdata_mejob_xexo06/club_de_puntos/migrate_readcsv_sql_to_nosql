const movimientoModel = require('../mongo/models/movimiento');
const csvtojson = require('csvtojson');
const path = require('path');

const findAddressRcursive = async (data, dateStartProccess) => {
    try {
        var contador;
        var next;
        var limit;
        //console.log('entro 0 ');
        const directoryPath = path.join(__dirname, '../export_movimientos.csv');
        console.log('s');
        if (data.lastAfter) {
            //console.log('data.lastAfter : ' + data.lastAfter);
            contador = data.lastAfter;
        } else {
            //console.log('no trae datos ');
            contador = 0;
        }

        if (data.next) {
            //console.log('data.next : ' + data.next);
            next = data.next;
        } else {
            //console.log('no trae datos ');
            next = true;
        }

        if (data.limit) {
            //console.log('data.limit ' + data.limit);
            limit = data.limit;
        } else {
            //console.log('no trae datos ');
            limit = 400;
        }
        console.log('s');
        csvtojson()
            .fromFile(directoryPath)
            .then((source) => {
                console.log('s');
                while (contador <= limit && next === true) {
                    //console.log('entro');
                    if (300000 === contador) {
                        //if (source.length === contador) {
                        //if (contador === 10) {
                        next = false;
                        var f = new Date();
                        console.log('START PROCCESS: ' + dateStartProccess);
                        console.log('FIN: ' + f);
                        console.log('contador: ' + contador);
                    } else {
                        let datosMovimiento = new movimientoModel();
                        let valor = source[contador]['TIPO_MOVIMIENTO'];
                        let arrTipoMov = valor.split(';');

                        datosMovimiento.tipoMovimiento.valor = arrTipoMov[0];
                        datosMovimiento.tipoMovimiento.desc = arrTipoMov[1];

                        //if (datosMovimiento.tipoMovimiento.desc == 'ABONO') {
                        if (datosMovimiento.tipoMovimiento.desc == 'ABONO') {
                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];
                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];
                            datosMovimiento.monto = source[contador]['MONTO'];
                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            //console.log('typeof fecha1 ' + typeof fecha1);
                            //console.log('typeof fecha1 ' + fecha1);
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            //solo existe un dato de 2 millones
                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');

                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            //datos que no se trabajan en abono
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            //console.log('typeof fecha2 ' + typeof fecha2);
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaInvalida2Parte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalida2Parte2 = fechaInvalida[1];
                                fechaValida2 =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();

                            //console.log('contador' + contador);
                        } else if (
                            datosMovimiento.tipoMovimiento.desc == 'ALTA'
                        ) {
                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];
                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];
                            datosMovimiento.monto = source[contador]['MONTO'];
                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            //console.log('typeof fecha1 ' + typeof fecha1);
                            //console.log('typeof fecha1 ' + fecha1);
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            //solo existe un dato de 2 millones
                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');

                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            //datos que no se trabajan en abono
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            //console.log('typeof fecha2 ' + typeof fecha2);
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaInvalida2Parte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalida2Parte2 = fechaInvalida[1];
                                fechaValida2 =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();
                        } else if (
                            datosMovimiento.tipoMovimiento.desc == 'AJUSTE'
                        ) {
                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];
                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];
                            datosMovimiento.monto = source[contador]['MONTO'];
                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            //console.log('typeof fecha1 ' + typeof fecha1);
                            //console.log('typeof fecha1 ' + fecha1);
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');

                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            //datos que no se trabajan en Ajuste
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            //console.log('typeof fecha2 ' + typeof fecha2);
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaInvalida2Parte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalida2Parte2 = fechaInvalida[1];
                                fechaValida2 =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();
                        } else if (
                            datosMovimiento.tipoMovimiento.desc == 'VENCIMIENTO'
                        ) {
                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];

                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];

                            datosMovimiento.monto = source[contador]['MONTO'];

                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            //console.log('typeof fecha1 ' + typeof fecha1);
                            //console.log('typeof fecha1 ' + fecha1);
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            // datos que no se trabajan en Vencimiento
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            //console.log('typeof fecha2 ' + typeof fecha2);
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaInvalida2Parte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalida2Parte2 = fechaInvalida[1];
                                fechaValida2 =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');

                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();

                            //console.log('contador' + contador);
                        } else if (
                            datosMovimiento.tipoMovimiento.desc == 'RESERVA'
                        ) {
                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];

                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];

                            datosMovimiento.monto = source[contador]['MONTO'];

                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            // datos que no se trabajan en Reserva
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaInvalida2Parte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalida2Parte2 = fechaInvalida[1];
                                fechaValida2 =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');

                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();

                            //console.log('contador' + contador);
                        } else if (
                            datosMovimiento.tipoMovimiento.desc == 'CANJE'
                        ) {
                            datosMovimiento.tipoMovimiento.valor = '12';
                            datosMovimiento.tipoMovimiento.desc = 'RESERVA';

                            datosMovimiento.idMov =
                                source[contador]['COD_MOVIMIENTO'];

                            datosMovimiento.rutCliente =
                                source[contador]['RUT_SOCIO'];

                            datosMovimiento.monto = source[contador]['MONTO'];

                            datosMovimiento.saldo = source[contador]['SALDO'];
                            datosMovimiento.usuario =
                                source[contador]['USUARIO_UNIF'];

                            let valor1 = source[contador]['COD_CANAL'];
                            if (valor1 !== '') {
                                let arrCanal = valor1.split(';');

                                datosMovimiento.codigoCanal.valor = arrCanal[0];
                                datosMovimiento.codigoCanal.desc = arrCanal[1];
                            } else {
                                datosMovimiento.codigoCanal = undefined;
                            }

                            var fecha1 = source[contador]['FEC_MOVIMIENTO'];
                            //console.log('typeof fecha1 ' + typeof fecha1);
                            //console.log('typeof fecha1 ' + fecha1);
                            if (fecha1 !== '') {
                                fechaInvalida = fecha1.split(' ');
                                fechaInvalidaParte1 =
                                    fechaInvalida[0].split('/');
                                fechaInvalidaParte2 = fechaInvalida[1];
                                fechaValida =
                                    '20' +
                                    fechaInvalidaParte1[2] +
                                    '-' +
                                    fechaInvalidaParte1[1] +
                                    '-' +
                                    fechaInvalidaParte1[0] +
                                    'T' +
                                    fechaInvalidaParte2;
                                datosMovimiento.fechaMovimiento = new Date(
                                    fechaValida
                                );
                            } else {
                                datosMovimiento.fechaMovimiento = null;
                            }

                            // datos que no se trabajan en CANJE
                            var fecha2 = source[contador]['FECHA_VIGENCIA'];
                            //console.log('typeof fecha2 ' + typeof fecha2);
                            if (fecha2 !== '') {
                                fechaInvalida2 = fecha2.split(' ');
                                fechaValida2 =
                                    fechaInvalida2[0] + 'T' + fechaInvalida2[1];
                                //console.log('fecha valida 2 ' + fechaValida2);
                                datosMovimiento.fechaVigencia = new Date(
                                    fechaValida2
                                );
                            } else {
                                datosMovimiento.fechaVigencia = undefined;
                            }

                            var motivo = source[contador]['COD_MOTIVO'];
                            if (motivo !== ';') {
                                let arrMotivo = motivo.split(';');
                                datosMovimiento.motivo.valor = arrMotivo[0];
                                datosMovimiento.motivo.desc = arrMotivo[1];
                            } else {
                                datosMovimiento.motivo = undefined;
                            }

                            var codigoProducto =
                                source[contador]['COD_PRODUCTO'];
                            if (codigoProducto !== '') {
                                datosMovimiento.codigoProducto =
                                    source[contador]['COD_PRODUCTO'];
                            } else {
                                datosMovimiento.codigoProducto = undefined;
                            }

                            var estadoReserva =
                                source[contador]['ESTADO_RESERVA'];
                            if (estadoReserva !== ';') {
                                let arrReserva = estadoReserva.split(';');
                                datosMovimiento.estadoReserva.valor =
                                    arrReserva[0];
                                datosMovimiento.estadoReserva.desc =
                                    arrReserva[1];
                            } else {
                                datosMovimiento.estadoReserva = undefined;
                            }

                            datosMovimiento.decripcionMovimiento =
                                source[contador]['OBS_MOVIMIENTO'];
                            datosMovimiento.estadoMigrado =
                                source[contador]['ESTADO_MIGRADO'];
                            datosMovimiento.cargaCampana =
                                source[contador]['CARGA_CAMPANA'];

                            datosMovimiento.save();

                            //console.log('contador' + contador);
                        }
                        // }
                    }
                    contador++;
                    console.log('contador' + contador);
                    //console.log('next' + next);
                }

                if (next === true) {
                    let variables = {
                        lastAfter: contador,
                        next: true,
                        limit: 200 + limit,
                    };
                    findAddressRcursive(variables, dateStartProcces);
                } else {
                    //console.log('fin del proceso');
                }
            });
    } catch (error) {
        console.log('ErrorCrearMovimiento: ' + error);
    }
};

module.exports = {
    findAddressRcursive,
};
