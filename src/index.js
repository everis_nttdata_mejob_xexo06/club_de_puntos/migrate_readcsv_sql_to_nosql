const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');

const movimientoController = require('./controllers/movimiento-controller');
const clienteController = require('./controllers/cliente-controller');
dotenv.config();

const routes = require('./routes');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

routes(app);

const { PORT, MONGODB_URL } = process.env;
console.log('mongo uri: ' + MONGODB_URL);

mongoose
    .connect(MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    })
    .then(() => {
        console.log('connected to mongodb');

        let variables = {
            lastAfter: 0,
            next: true,
            limit: 100000,
        };
        var dateStartProccess = new Date();
        movimientoController.findAddressRcursive(variables, dateStartProccess);
        //clienteController.findAddressRcursive(variables, dateStartProccess);
        app.listen(PORT, () => {
            console.log(`running on ${PORT}`);
        });
    })
    .catch((error) => {
        console.log('mongdb error', error);
    });
