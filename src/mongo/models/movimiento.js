const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const uniqueValidator = require('mongoose-unique-validator');

let SepMov = new Schema({
    _id: {
        type: 'string',
    },
    seq_movimiento: {
        type: 'number',
        default: 0,
    },
});

let counter = mongoose.model('seqmovimiento', SepMov);

let MovimientoSchema = new Schema({
    idMov: {
        type: 'number',
        unique: true,
    },
    rutCliente: {
        type: 'string',
    },
    monto: {
        type: 'number',
    },
    saldo: {
        type: 'number',
        default: 0,
    },
    usuario: {
        type: 'string',
    },
    codigoProducto: {
        type: 'string',
    },
    estadoReserva: {
        valor: {
            type: 'string',
            default: null,
        },
        desc: {
            type: 'string',
            default: null,
        },
    },
    fechaMovimiento: {
        type: 'date',
    },
    fechaVigencia: {
        type: 'date',
    },
    decripcionMovimiento: {
        type: 'string',
        default: null,
    },
    estadoMigrado: {
        type: 'string',
        default: null,
    },
    cargaCampana: {
        type: 'string',
        default: null,
    },

    tipoMovimiento: {
        valor: {
            type: 'string',
            default: null,
        },
        desc: {
            type: 'string',
            default: null,
        },
    },
    codigoCanal: {
        valor: {
            type: 'string',
            default: null,
        },
        desc: {
            type: 'string',
            default: null,
        },
    },
    motivo: {
        valor: {
            type: 'string',
            default: null,
        },
        desc: {
            type: 'string',
            default: null,
        },
    },
});

MovimientoSchema.plugin(uniqueValidator);

MovimientoSchema.pre('save', function (next) {
    //console.log('MovimientoSchema.pre');
    var doc = this;
    counter
        .findOneAndUpdate(
            { _id: 'entityId' },
            { $inc: { seq_movimiento: 1 } },
            { new: true, upsert: true }
        )
        .then(function (count) {
            //console.log('...count: ' + JSON.stringify(count));
            doc.idMov = count.seq_movimiento;
            next();
        })
        .catch(function (error) {
            //console.error('counter error-> : ' + error);
            throw error;
        });
});
module.exports = mongoose.model('movimientos2', MovimientoSchema);
