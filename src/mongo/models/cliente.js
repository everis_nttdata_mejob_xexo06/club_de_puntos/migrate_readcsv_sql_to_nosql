const mongoose = require('mongoose');

const { Schema } = mongoose;

const uniqueValidator = require('mongoose-unique-validator');

const clientechema = new Schema({
    rutCliente: {
        type: String,
        unique: true,
    },
    saldo: {
        type: Number,
        default: null,
    },
    fechaultimaMod: {
        type: Date,
        default: null,
    },
    fechaCreacion: {
        type: Date,
        default: null,
    },
    estadoMigrado: {
        type: String,
        default: null,
    },
});

clientechema.plugin(uniqueValidator);

const model = mongoose.model('cliente', clientechema);

module.exports = model;
